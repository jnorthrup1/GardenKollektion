@file:Suppress("FunctionName", "KDocMissingDocumentation")

package gk.kademlia.fsm

import vec.macros.*
import java.nio.channels.SelectionKey

typealias KeyAction = (SelectionKey) -> FsmNode?

/**like RFC822 smtp message*/
typealias SimpleMessage = Pai2<Vect0r<Pai2<String, String>>, String>
typealias ReifiedMessage = Pair<List<Pair<String, String>>, String>

val SimpleMessage.reify: ReifiedMessage get() = first.toList().map { it.pair } to second
val ReifiedMessage.virtualize: SimpleMessage get() = first α { Tw1n(it) } t2 second