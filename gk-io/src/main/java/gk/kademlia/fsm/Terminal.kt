package gk.kademlia.fsm

import java.nio.channels.SelectionKey

class Terminal(private val housekeeping: ((SelectionKey) -> Unit)? =  ::defaultCloseOp) : FsmNode {
    override val interest: Int = (0x7fff_ffff)
    override val process: KeyAction = {
        housekeeping?.invoke(it)
        null
    }

    companion object {
        fun defaultCloseOp(s: SelectionKey) {
            s.cancel()
            s.channel().close()
        }

    }
}