package gk.kademlia.io

import vec.macros.Pai2
import vec.macros.t2
import vec.macros.t3
import java.nio.ByteBuffer

enum class ContainerChunks {

    /**
     * "LIST"
     */
    LIST {
        @Suppress("UsePropertyAccessSyntax")
        override fun seq(buf: ByteBuffer): Sequence<Chunk> = sequence<Chunk> {
            var p1 = buf.slice()
            do {
                var sz: Int
                yield(String(ByteArray(4).also { p1.get(it) }, Charsets.UTF_8)
                        t2 p1.getInt().also { sz = it }
                        t3 p1.slice().limit(sz))
                p1 = p1.position(p1.position() + sz).slice()
            } while (p1.remaining() >= 8)
        }

        override fun contents(buf: ByteBuffer): List<Pai2<String, Int>> = seq(buf).map { (a, b) -> a t2 b }.toList()
    },
    ;


    abstract fun contents(buf: ByteBuffer): List<Pai2<String, Int>>
    abstract fun seq(buf: ByteBuffer): Sequence<Chunk>
}