package gk.kademlia.io

import vec.macros.Tripl3
import java.nio.ByteBuffer

/**
IFF Chunk Spec http://www.martinreddy.net/gfx/2d/IFF.txt
```!C
typedef struct {
UBYTE[4] ckID;
UInt	 ckSize;	/* sizeof(ckData) */
UBYTE	 ckData[/* ckSize */];
} Chunk;
``` */

typealias Chunk = Tripl3<String, Int, ByteBuffer>
