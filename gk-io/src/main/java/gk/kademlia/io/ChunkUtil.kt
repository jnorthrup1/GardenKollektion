package gk.kademlia.io

import gk.kademlia.codec.SmCodec
import gk.kademlia.fsm.*
import vec.macros.Tripl3
import vec.macros.t2
import vec.macros.t3
import java.nio.ByteBuffer
import java.nio.channels.ByteChannel
import java.nio.channels.SelectionKey

object ChunkUtil {
    val ByteBuffer.fromChunk: SimpleMessage?
        get() = SmCodec.recv(apply { long }.slice())

    /**
     * reads a chunk.  when the chunk is done the "yeild" is called
     */
    @JvmStatic
    @JvmOverloads
    fun readChunk(yeild: ((Chunk) -> Unit)?, then: FsmNode? = Terminal()): ReadNode = run {
        lateinit var typ: String
        var ckSize: Int
        var buf: ByteBuffer = ByteBuffer.allocate(8)
        ReadNode { key ->
            (key.channel() as ByteChannel).let { chan ->
                chan.read(buf)
                if (buf.hasRemaining()) null else {
                    buf.flip()
                    val byteArray = ByteArray(4)
                    buf.get(byteArray)
                    typ = String(byteArray, Charsets.UTF_8)
                    ckSize = buf.int
                    buf = when {
                        ckSize > 512 -> ByteBuffer.allocateDirect(ckSize)
                        else -> ByteBuffer.allocate(ckSize)
                    }
                    ReadNode {
                        chan.read(buf)
                        if (buf.hasRemaining()) null
                        else {
                            yeild?.invoke(typ t2 ckSize t3 buf.flip())
                            then
                        }
                    }
                }
            }
        }
    }

    val SimpleMessage.toChunk: Chunk
        get() = SmCodec.send(this).run { Tripl3("BYTE", limit(), this) }

    @JvmStatic
    @JvmOverloads
    fun writeChunk(chunk: Chunk, then: FsmNode? = Terminal()): WriteNode = ByteBuffer.allocate(8).apply {
        val bytes = chunk.first.toByteArray().sliceArray(0..3)
        this.put(bytes)
        repeat(4 - bytes.size) { this.put(' '.code.toByte()) }
        this.putInt(chunk.second)
    }.flip().let { buf: ByteBuffer ->
        WriteNode { key: SelectionKey ->
            (key.channel() as ByteChannel).let { chan ->
                chan.write(buf)
                val block = chunk.third.rewind()
                chan.write(block)
                if (block.hasRemaining()) {
                    WriteNode {
                        chan.write(block)
                        if (block.hasRemaining()) null
                        else then
                    }
                } else then
            }
        }
    }
}