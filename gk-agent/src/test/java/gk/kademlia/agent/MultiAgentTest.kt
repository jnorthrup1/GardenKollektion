package gk.kademlia.agent


import gk.kademlia.agent.fsm.Chunk
import gk.kademlia.io.ReadChunk
import gk.kademlia.fsm.FSM
import java.util.concurrent.ExecutorService
import kotlin.test.Test

class MultiAgentTest {

    private var fsm: FSM
    private var executorService: ExecutorService
    fun doSomething(chunk: Chunk) {}

    init {
        val (executorService, fsm) = FSM.launch(ReadChunk(::doSomething))
        this.executorService = executorService
        this.fsm = fsm
    }

    @Test
    fun testMAAF() {
        fsm.topLevel

    }


}