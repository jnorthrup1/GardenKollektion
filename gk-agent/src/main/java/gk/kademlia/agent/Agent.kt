package gk.kademlia.agent


import com.sun.nio.sctp.SctpServerChannel
import gk.kademlia.agent.impl.WorldNetwork.fromBitClock
import gk.kademlia.bitops.BitOps.Companion.bitClockFromByteArray
import gk.kademlia.id.NUID
import gk.kademlia.include.Route
import gk.kademlia.net.NetMask
import gk.kademlia.routing.RoutingTable
import vec.util._l
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.URI
import java.net.UnixDomainSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.DatagramChannel
import java.nio.channels.NetworkChannel
import java.nio.channels.SelectableChannel

interface Agent<TNum : Comparable<TNum>, Sz : NetMask<TNum>> {
    /**
     * Network Unique Id
     */
    val nuid: NUID<TNum>

    /**
     * ipfs-addrs:

    # IPFS over TCP over IPv6 (typical TCP)
    /ip6/fe80::8823:6dff:fee7:f172/tcp/4001/ipfs/QmYJyUMAcXEw1b5bFfbBbzYu5wyyjLMRHXGUkCXpag74Fu

    # IPFS over uTP over UDP over IPv4 (UDP-shimmed transport)
    /ip4/162.246.145.218/udp/4001/utp/ipfs/QmYJyUMAcXEw1b5bFfbBbzYu5wyyjLMRHXGUkCXpag74Fu

    # IPFS over IPv6 (unreliable)
    /ip6/fe80::8823:6dff:fee7:f172/ipfs/QmYJyUMAcXEw1b5bFfbBbzYu5wyyjLMRHXGUkCXpag74Fu

    # IPFS over TCP over IPv4 over TCP over IPv4 (proxy)
    /ip4/162.246.145.218/tcp/7650/ip4/192.168.0.1/tcp/4001/ipfs/QmYJyUMAcXEw1b5bFfbBbzYu5wyyjLMRHXGUkCXpag74Fu

    # IPFS over Ethernet (no IP)
    /ether/ac:fd:ec:0b:7c:fe/ipfs/QmYJyUMAcXEw1b5bFfbBbzYu5wyyjLMRHXGUkCXpag74Fu

     */
    fun route(sc: SelectableChannel): Route<TNum> {
        var location: String? = null
        var port: Int? = null
        var proto: String? = null
        (sc as? SelectableChannel)?.let {
            var addr: InetAddress? = null
            proto = when (sc) {
                is NetworkChannel -> sc.let { chan ->
                    val localAddress = chan.localAddress
                    when (localAddress) {
                        is InetSocketAddress -> {
                            addr = localAddress.address
                            port = localAddress.port
                            location = localAddress.hostString
                        }
                    }
                    when {
                        chan is DatagramChannel -> "udp"
                        (localAddress is UnixDomainSocketAddress) -> localAddress.let {
                            location = it.path.toString()
                            "unix"
                        }
                        else -> "tcp"
                    }
                }
                is SctpServerChannel -> sc.let {
                    val localAddress = it.allLocalAddresses.firstOrNull()
                    when (localAddress) {
                        is InetSocketAddress -> {
                            addr = localAddress.address
                            port = localAddress.port
                            location = localAddress.hostString
                        }
                    }
                    "sctp"
                }
                else -> TODO()
            }
        }
        val address = _l[proto, location, port].mapNotNull { it.toString() }.joinToString(":", ("urn:")).let { URI(it) }
        return Route(nuid, address)
    }

    val routingTable: RoutingTable<TNum, Sz>
    fun readRoute(buf: ByteBuffer) {
        val uidBytes = ByteArray(nuid.netmask.bytes)

        buf.get(uidBytes)

        val maxbits = nuid.netmask.bits
        fromByeArray(uidBytes, maxbits)

        String(buf.slice().let { slice -> ByteArray(slice.remaining()).also { slice.get(it) } }, Charsets.UTF_8)


    }

    fun fromByeArray(uidBytes: ByteArray, maxbits: Int) {
        val clock1 = bitClockFromByteArray(uidBytes, maxbits)
         fromBitClock(*clock1)
    }

}
