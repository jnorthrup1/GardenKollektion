package gk.kademlia.agent.impl

import gk.kademlia.agent.Agent
import gk.kademlia.id.NUID
import gk.kademlia.routing.RoutingTable
import java.math.BigInteger

class WorldAgent(
    override val nuid: NUID<BigInteger>,
    override val routingTable: RoutingTable<BigInteger, WorldNetwork>,

    ) : Agent<BigInteger, WorldNetwork>