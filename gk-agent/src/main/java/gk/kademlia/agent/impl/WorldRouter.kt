package gk.kademlia.agent.impl

import gk.kademlia.id.NUID
import gk.kademlia.routing.RoutingTable
import java.math.BigInteger


class WorldRouter(agentNUID: NUID<BigInteger>) : RoutingTable<BigInteger, WorldNetwork>(agentNUID)

