package gk.kademlia.agent.impl

import gk.kademlia.net.NetMask
import java.math.BigInteger

object WorldNetwork : NetMask<BigInteger> {
    override val bits: Int
        get() = 160
}